
#!/usr/bin/python3

#
# Simple XML parser for JokesXML
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
# September 2009
#
# Just prints the jokes in a JokesXML file

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import urllib

def normalize_whitespace(text):
    "Remove redundant whitespace from a string"
    return " ".join(text.split())

class CounterHandler(ContentHandler):

    def __init__ (self):
        self.inContent = 0
        self.theContent = ""

    def startElement (self, name, attrs):
        if name == 'item':
            print( '<span>')
        elif name == 'title':
            self.inContent = 1
        elif name == 'link':
            self.inContent = 1
            
    def endElement (self, name):
        if self.inContent:
            self.theContent = normalize_whitespace(self.theContent)
        if name == 'item':
            print('</span>')
        elif name == 'title':
            print("<h2> " + self.theContent + "</h2>")
        elif name == 'link':
            print (" <a href= " +self.theContent + ">" + self.theContent + "</a>")
        if self.inContent:
            self.inContent = 0
            self.theContent = ""
        
    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars
            
# --- Main prog

    
# Load parser and driver

JokeParser = make_parser()
JokeHandler = CounterHandler()
JokeParser.setContentHandler(JokeHandler)

# Ready, set, go!

xmlFile = urllib.request.urlopen('http://barrapunto.com/index.rss')
JokeParser.parse(xmlFile)

